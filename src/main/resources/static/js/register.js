$(document).ready(function(){
	
	loginCorrect = false;
	retypePasswordEdit = false;
	
	$("#register").addClass("disabled");

	var replaceTooltipText = function(el, text) {
	    if(el.data("tooltip-show")) {
            el.next().find(".tooltip-inner").text(text);
	    } else {
            el.attr("title", text);
	    }
	}
	
	var isError = function(el) {
		if(!el.parent().hasClass("has-error")) 
			el.parent().addClass("has-error");
		if(el.parent().hasClass("has-success")) 
			el.parent().removeClass("has-success");
		if(!el.data("tooltip-show")) {
			el.tooltip("show");
			el.data("tooltip-show", true);
		}
		if(el.parent().find("span").hasClass("glyphicon-ok")) 
			el.parent().find("span").removeClass("glyphicon-ok");
		if(!el.parent().find("span").hasClass("glyphicon-remove"))
			el.parent().find("span").addClass("glyphicon-remove");
		if(el.parent().find("span").css("display") == "none")
			el.parent().find("span").show();
	}
	
	var isSuccess = function(el) {
		if(el.parent().hasClass("has-error")) 
			el.parent().removeClass("has-error");
		if(!el.parent().hasClass("has-success")) 
			el.parent().addClass("has-success");
		if(el.parent().find("span").hasClass("glyphicon-remove")) 
			el.parent().find("span").removeClass("glyphicon-remove");
		if(!el.parent().find("span").hasClass("glyphicon-ok")) 
			el.parent().find("span").addClass("glyphicon-ok");
		if(el.parent().find("span").css("display") == "none")
			el.parent().find("span").show();
		if(el.data("tooltip-show")) {
			el.tooltip("hide");
			el.data("tooltip-show", false);
		}
	}
	
	var isEmpty = function(el) {
		el.parent().removeClass("has-error");
		el.parent().removeClass("has-success");
		el.tooltip("hide")
		el.parent().find("span").hide();
	}
	
	var passwordEquals = function(eventObject ) {
		if(!retypePasswordEdit && $("#retype-password").val() == "")
			return true;
		
		if(!retypePasswordEdit && $("#retype-password").val() != "")
			retypePasswordEdit = true;
		
		if($("#password").val() == "" && $("#retype-password").val() == "") {
			isEmpty($("#retype-password"));
			return false;
		}
		
		if($("#password").val() != $("#retype-password").val()) {
			isError($("#retype-password"));
			return false;
		} else {
			isSuccess($("#retype-password"));
			return true;
		}
		
	}
	
	var activateButton = function() {
		if(passwordEquals() && loginCorrect) {
			$("#register").removeClass("disabled");
			return true;
		} else {
			$("#register").addClass("disabled")
			return false;
		}
			
	}
	
	var inputEmpty = function() {
		if($(this).val() != "") {
			$(this).parent().removeClass("has-error");
			$(this).parent().addClass("has-success");
			$(this).parent().find("span").removeClass("glyphicon-remove");
			$(this).parent().find("span").addClass("glyphicon-ok")
			$(this).parent().find("span").show();			
		} else {
			$(this).parent().removeClass("has-success");
			$(this).parent().find("span").hide();	
		}
	}
	

	checkLogin = function() {
		if($("#email").val() == "") {
			isEmpty($("#email"));
			loginCorrect = false;
			return;
		}
		
		if(!validEmail()) {
			var tooltipTextError = "Email not valid!";
			replaceTooltipText($("#email"), tooltipTextError)
//			$("#email").attr("title", tooltipTextError);
			isError($("#email"));
			loginCorrect = false;
			return
		}
		
		$.ajax({
			url : "/register/checkLoginUnique",
			data : {
				login : $(this).val()
			}
		}).done(function(data) {
			if (!data.isUnique) {
			    var tooltipTextError = "Email already exist!";
                replaceTooltipText($("#email"), tooltipTextError)
				isError($("#email"));
				loginCorrect = false;
				activateButton();
			} else {
				isSuccess($("#email"));
				loginCorrect = true;
				activateButton();
			}
		})
	}
	
	var validEmail = function() {
		var regExp = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/ig;
		var result = regExp.exec($("#email").val());
		if(result != null) 
			return true;
		else
			return false;
	}
	

	$("input:not(#email)").focusout(inputEmpty);
	$("#password").focusout(activateButton);
	$("#retype-password").focusout(activateButton);
	
	$("#email").focusout(checkLogin);
})