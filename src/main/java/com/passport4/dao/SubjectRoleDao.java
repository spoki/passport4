package com.passport4.dao;

import com.passport4.domain.SubjectRole;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRoleDao extends CrudRepository<SubjectRole, Long> {
}
