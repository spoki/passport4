package com.passport4.dao;

import com.passport4.domain.Subject;
import org.springframework.data.repository.CrudRepository;

public interface SubjectDao extends CrudRepository<Subject, Long> {

    public Subject findByLogin(String login);

    public Subject findByExtCode(String extCode);
}
