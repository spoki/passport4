package com.passport4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.session.*;
import org.springframework.security.web.session.ConcurrentSessionFilter;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/css/**", "/img/**", "/js/**", "/fonts/**", "/register/**").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                //.antMatchers("/profile/**").hasRole("USER")
                .anyRequest().authenticated()
            .and()
            .sessionManagement().sessionFixation().newSession()
            .and()
            .formLogin().loginPage("/login").permitAll()
            .and()
            .logout().logoutUrl("/logout");

        http.sessionManagement().sessionAuthenticationStrategy(getSessionStrategy());
        http.addFilter(new ConcurrentSessionFilter(sessionRegistry, "/login"));
    }

    @Bean
    public CompositeSessionAuthenticationStrategy getSessionStrategy() {
        List<SessionAuthenticationStrategy> list = new ArrayList<>();

        ConcurrentSessionControlAuthenticationStrategy strategy =
                new ConcurrentSessionControlAuthenticationStrategy(sessionRegistry);
        strategy.setMaximumSessions(4);
        strategy.setExceptionIfMaximumExceeded(true);
        list.add(strategy);
        list.add(new SessionFixationProtectionStrategy());
        list.add(new RegisterSessionAuthenticationStrategy(sessionRegistry));
        return new CompositeSessionAuthenticationStrategy(list);
    }

    @Bean
    public SessionRegistry getSessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder;
    }
}