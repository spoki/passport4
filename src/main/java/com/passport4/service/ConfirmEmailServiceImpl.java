package com.passport4.service;

import com.passport4.dao.SubjectDao;
import com.passport4.domain.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class ConfirmEmailServiceImpl implements ConfirmEmailService {

    @Autowired
    private SubjectDao subjectDao;

    @Value("${mail.username}")
    private String mailUsername;

    @Value("${mail.password}")
    private String mailPassword;

    @Value("${mail.smtp.auth}")
    private String mailSmtpAuth;

    @Value("${mail.smtp.starttls.enable}")
    private String mailSmtpStartTlsEnable;

    @Value("${mail.smtp.host}")
    private String mailSmtpHost;

    @Value("${mail.smtp.port}")
    private String mailSmtpPort;

    @Value("${mail.from}")
    private String mailFrom;

    @Override
    public void sendConfirmEmail(final Subject subject) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Properties props = new Properties();
                props.put("mail.smtp.auth", mailSmtpAuth);
                props.put("mail.smtp.starttls.enable", mailSmtpStartTlsEnable);
                props.put("mail.smtp.host", mailSmtpHost);
                props.put("mail.smtp.port", mailSmtpPort);

                Session session = Session.getInstance(props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(mailUsername, mailPassword);
                            }
                        });

                try {
                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(mailFrom));
                    message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(subject.getLogin()));
                    message.setSubject("Passport4 register - confirm email");
                    message.setText("Passport4 account info:\n\n" +
                            "Login: " + subject.getLogin() + "\n" +
                            "E-mail confirm code: " + subject.getEmailConfirmCode() + "\n" +
                            "E-mail confirm link: http://localhost:8088/register/emailConfirmation/" +
                            subject.getExtCode() + "/" + subject.getEmailConfirmCode());

                    Transport.send(message);

                    System.out.println("Send confirm email: [" + subject.getLogin() + "]");

                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        thread.start();
    }

    @Override
    @Transactional
    public ConfirmedStatus confirm(Subject subject, String confirmCode) {

        if(subject.getConfirmed())
            return ConfirmedStatus.EXIST;

        if (subject == null) {
            return ConfirmedStatus.ERROR;
        }

        if (subject.getEmailConfirmCode().equals(confirmCode)) {
            subject.setConfirmed(true);
            subjectDao.save(subject);
            return ConfirmedStatus.DONE;
        } else {
            return ConfirmedStatus.ERROR;
        }
    }

}
