package com.passport4.service;

import com.passport4.domain.Subject;

public interface ConfirmEmailService {

    public void sendConfirmEmail(Subject subject);

    public ConfirmedStatus confirm(Subject subject, String confirmCode);

}
