package com.passport4.service;

import com.passport4.dao.SubjectDao;
import com.passport4.domain.Subject;
import com.passport4.domain.SubjectRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private SubjectDao subjectDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Subject subject = subjectDao.findByLogin(username);
        return buildUserDetailsBySubject(subject);
    }

    private UserDetails buildUserDetailsBySubject(Subject subject) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (SubjectRole role : subject.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new User(
                subject.getLogin(),
                subject.getPassword(),
                subject.getEnabled(),
                true,
                true,
                true,
                authorities);
    }
}
