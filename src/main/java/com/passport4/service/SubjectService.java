package com.passport4.service;

import com.passport4.domain.Subject;
import org.springframework.security.core.AuthenticationException;

public interface SubjectService {

    public Subject findByLogin(String login);

    public Subject findByExtCode(String subjectExtCode);

    public void createNew(Subject subject);

    public void save(Subject subject);

    public void changePassword(String oldPassword, String newPassword, String sessionId) throws AuthenticationException;
}
