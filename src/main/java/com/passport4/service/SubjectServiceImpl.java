package com.passport4.service;

import com.passport4.dao.SubjectDao;
import com.passport4.dao.SubjectRoleDao;
import com.passport4.domain.Subject;
import com.passport4.domain.SubjectRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectDao subjectDao;

    @Autowired
    private SubjectRoleDao subjectRoleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    @Transactional(readOnly = true)
    public Subject findByLogin(String login) {
        return subjectDao.findByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public Subject findByExtCode(String subjectExtCode) {
        return subjectDao.findByExtCode(subjectExtCode);
    }

    @Override
    @Transactional
    public void createNew(Subject subject) {
        subject.setPassword(passwordEncoder.encode(subject.getPassword()));
        subject.setEnabled(true);
        subject.setExtCode(UUID.randomUUID().toString());
        subject.setConfirmed(false);
        subject.setEmailConfirmCode(UUID.randomUUID().toString());
        subjectDao.save(subject);

        if (subject.getRoles().isEmpty()) {
            SubjectRole role = new SubjectRole();
            role.setName("USER");
            role.setSubject(subject);
            subjectRoleDao.save(role);
            subject.getRoles().add(role);
        }
    }

    @Override
    @Transactional
    public void save(Subject subject) {
        subjectDao.save(subject);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword, String sessionId) throws AuthenticationException  {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Subject subject = subjectDao.findByLogin(auth.getName());

        if(!passwordEncoder.matches(oldPassword, subject.getPassword()))
            throw new BadCredentialsException("Illegal old password");

        String encodedNewPassword = passwordEncoder.encode(newPassword);

        UserDetails userDetails = new User(subject.getLogin(), "", Collections.emptyList());

        List<SessionInformation> sessionsList = sessionRegistry.getAllSessions(userDetails, false);

        for(SessionInformation sessionInformation : sessionsList) {
            if(!sessionInformation.getSessionId().equals(sessionId))
                sessionInformation.expireNow();
        }

        subject.setPassword(encodedNewPassword);
        subjectDao.save(subject);
    }
}
