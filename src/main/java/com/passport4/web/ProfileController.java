package com.passport4.web;

import com.passport4.domain.Subject;
import com.passport4.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
public class ProfileController {

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/profile/{subjectExtCode}")
    public String profile(
            @PathVariable(value = "subjectExtCode") String subjectExtCode,
            Principal principal,
            ModelMap model) {

        if (principal == null || principal.getName() == null || principal.getName().isEmpty()) {
            return "redirect:/login";
        }

        String subjectLogin = principal.getName();
        Subject subject = subjectService.findByLogin(subjectLogin);

        if (!subject.getExtCode().equals(subjectExtCode)) {
            return "redirect:/profileAuthError";
        }
        if (!subject.getConfirmed()) {
            return "redirect:/login/unconfirmed";
        }

        model.addAttribute("subject", subject);
        return "profile";
    }

    public String changePassword() {
        return null;
    }

    @RequestMapping(value = "profile/edit")
    public String editProfile(Subject editSubject, Principal principal, ModelMap model) {

        Subject subject = subjectService.findByLogin(principal.getName());

        boolean change = false;

        if(editSubject.getFirstName() != null && !editSubject.getFirstName().trim().equals("")) {
            subject.setFirstName(editSubject.getFirstName());
            change = true;
        }

        if(editSubject.getSecondName() != null && !editSubject.getSecondName().trim().equals("")) {
            subject.setSecondName(editSubject.getSecondName());
            change = true;
        }

        if(editSubject.getSurname() != null && !editSubject.getSurname().trim().equals("")) {
            subject.setSurname(editSubject.getSurname());
            change = true;
        }
        if(change) {
            subjectService.save(subject);
            return "redirect:/profile/" + subject.getExtCode();
        }

        model.addAttribute("subject", subject);
        return "editProfile";
    }

    @RequestMapping(value = "profile/editpassword")
    public String editPassword(@RequestParam(required = false) String oldPassword,
                               @RequestParam(required = false) String newPassword,
                               ModelMap model, HttpSession httpSession, Principal principal) {

        model.put("login", principal.getName());

        if(oldPassword != null && !oldPassword.trim().equals("") &&
                newPassword != null && !newPassword.trim().equals("")) {
            try {
                subjectService.changePassword(oldPassword, newPassword, httpSession.getId());
                Subject subject = subjectService.findByLogin(principal.getName());
                return "redirect:/profile/" + subject.getExtCode();
            } catch(AuthenticationException e) {
//                e.printStackTrace();
                model.put("error", true);
            }
        }

        return "editPassword";
    }



    @RequestMapping(value = "/profileAuthError")
    public String profileAuthError() {
        return "profileAuthError";
    }
}
