package com.passport4.web;

import com.passport4.domain.Subject;
import com.passport4.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private ConfirmEmailService confirmEmailService;

    @RequestMapping(value = "")
    public String register(ModelMap model) {
        model.addAttribute("subject", new Subject());
        return "register";
    }

    @RequestMapping(value = "/checkLoginUnique", method = RequestMethod.GET)
    @ResponseBody
    public Object checkLoginUnique(@RequestParam(value = "login") final String login, HttpSession httpSession) {
        Subject subject = subjectService.findByLogin(login);
        final boolean isUnique = (subject == null) ? true : false;
        return new Object() {
            public String getLogin() { return login; }
            public Boolean getIsUnique() { return isUnique; }
        };
    }

    @RequestMapping(value = "/save")
    public String save(Subject subject, ModelMap model) {
        subjectService.createNew(subject);
        confirmEmailService.sendConfirmEmail(subject);
        return "redirect:/register/confirmEmail/" + subject.getExtCode();
    }

    @RequestMapping(value = "/confirmEmail/{subjectExtCode}")
    public String registerConfirmEmail(
            @PathVariable String subjectExtCode,
            @RequestParam (value = "confirmCode", required = false) String confirmCode,
            ModelMap model) {

        Subject subject = subjectService.findByExtCode(subjectExtCode);
        model.addAttribute("subject", subject);

        if(confirmCode != null) {
            ConfirmedStatus status = confirmEmailService.confirm(subject, confirmCode);

            switch(status) {
                case DONE:
                    return "redirect:/register/done";
                case EXIST:
                    return "redirect:/login";
                case ERROR:
                    model.put("errorConfirm", true);
                    return "registerConfirmEmail";
            }
        }

        return "registerConfirmEmail";
    }

    @RequestMapping(value = "/emailConfirmationError")
    public String emailConfirmationError() {
        return "registerEmailConfirmationError";
    }

    @RequestMapping(value = "/done")
    public String registerDone() {
        return "registerDone";
    }

}
