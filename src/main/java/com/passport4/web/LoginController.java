package com.passport4.web;

import com.passport4.domain.Subject;
import com.passport4.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class LoginController {

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/")
    public String index() {
        return "redirect:/login";
    }

    @RequestMapping(value = "/login")
    public String login(Principal principal, ModelMap model) {
        if (principal == null) {
            return "login";
        } else {
            String subjectLogin = (principal != null) ? principal.getName() : "";
            Subject subject = subjectService.findByLogin(subjectLogin);
            if (!subject.getConfirmed()) {
                return "redirect:/login/unconfirmed";
            }
            model.addAttribute("subject", subject);
            return "redirect:/profile/" + subject.getExtCode();
        }
    }

    @RequestMapping(value = "/login/unconfirmed")
    public String unconfirmed() {
        return "loginUnconfirmed";
    }
}
