package com.passport4.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "SUBJECT")
public class Subject {

    @Id
    @Column(name = "SUBJECT_ID", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "LOGIN", unique = true, nullable = false, length = 100)
    private String login;

    @Column(name = "PASSWD", unique = false, nullable = false, length = 100)
    private String password;

    @Column(name = "IS_ENABLED", nullable = false)
    private Boolean enabled;

    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    private Set<SubjectRole> roles = new HashSet<SubjectRole>(0);

    @Column(name = "EXT_CODE", unique = true, nullable = false, length = 100)
    private String extCode;

    @Column(name = "FIRST_NAME", unique = false, nullable = false, length = 100)
    private String firstName;

    @Column(name = "SECOND_NAME", unique = false, nullable = false, length = 100)
    private String secondName;

    @Column(name = "SURNAME", unique = false, nullable = false, length = 100)
    private String surname;

    @Column(name = "EMAIL_CONFIRM_CODE", unique = false, nullable = true, length = 100)
    private String emailConfirmCode;

    @Column(name = "IS_CONFIRMED", nullable = false)
    private Boolean confirmed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<SubjectRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<SubjectRole> roles) {
        this.roles = roles;
    }

    public String getExtCode() {
        return extCode;
    }

    public void setExtCode(String extCode) {
        this.extCode = extCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmailConfirmCode() {
        return emailConfirmCode;
    }

    public void setEmailConfirmCode(String emailConfirmCode) {
        this.emailConfirmCode = emailConfirmCode;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }
}
